# 蜻蜓记账本

#### 介绍
##### 蜻蜓小记，是一个 基于java ，springboot的微信小程序开源账本。记账小程序。可二次开发，完全开源。QQ群:690051325（群文件下载sql运行脚本）

##### 线上体验
<img width="30%" src="https://andwky.top:9000/avatar/6aa7f64d-28fd-4252-800f-0e8bb34205dc_tmp_2e690635ffd285beace15a4f4b2e0acd.jpg"/><br/>
#### 系统截图
![输入图片说明](%E9%A6%96%E9%A1%B5.png)
![输入图片说明](%E8%AE%B0%E4%B8%80%E7%AC%94.png)
![输入图片说明](%E8%B4%A6%E6%9C%AC%E5%88%97%E8%A1%A8.png)
![输入图片说明](%E8%B4%A6%E6%9C%AC%E8%AF%A6%E6%83%85.png)
![输入图片说明](%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%831.png)


#### 软件架构

1.  springboot+mybatisplus项目，数据库mysql，存储服务使用自建ftp+nginx反向代理到ftp目录访问
2.  小程序端使用微信传统开发
3.  缓存采用redis


#### 使用说明

1.  bookkeeping-parent为springboot项目，直接打卡运行即可
2.  bookkeeping-mini 为微信小程序
3.  spire.xls.free-2.2.0 为excel转图片工具jar
4.  doc sql等文档


#### 开源不易，如果你觉得不错，请给个star吧，对了记得加好友拉群取群文件：QQ群 690051325
![输入图片说明](4d3d30c98482935a473f0177cd1eeeb.jpg)



